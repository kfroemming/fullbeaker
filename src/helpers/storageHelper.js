export const setItem = (key, obj) => {
  localStorage.setItem(key, obj);
};

export const get = (key) => {
  return localStorage.getItem(key) || "";
};
