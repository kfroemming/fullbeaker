import { categories } from "./data";
import { render } from "@testing-library/react";
import App from "./App";
import React from "react";

JSON.parse = jest.fn().mockImplementationOnce(() => {
  return [{ id: "id", previewURL: "preview" }];
});

test("renders search", () => {
  const { getByTestId } = render(<App />);
  const search = getByTestId("SearchButton");
  expect(search).toBeInTheDocument();
});

test("renders categories", () => {
  const { getByLabelText } = render(<App />);
  const categories = getByLabelText("Categories");
  expect(categories).toBeInTheDocument();
});

test("categories", () => {
  expect(categories.length).toBe(20);
});
