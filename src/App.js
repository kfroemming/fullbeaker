import { categories } from "./data";
import { CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Column from "./components/Column";
import ErrorBoundary from "./components/ErrorBoundary";
import React from "react";
import SearchForm from "./components/SearchForm";
import SavedLinks from "./components/SavedLinks";
import { setItem, get } from "./helpers/storageHelper";
import MoodBadIcon from "@material-ui/icons/MoodBad";
import SearchResults from "./components/SearchResults";
import useFetch from "./hooks/useFetch";

import "./App.css";

/**
 * Secret key for the API
 */

const SECRET_KEY = "13136421-266c28a6d61717bc2e4e6a83e";

/**
 * Materiaul ui theme hook
 */
export const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  button: {
    borderRadius: "0",
  },
}));

function App() {
  const classes = useStyles();
  /**
   * define a few needed variables in state to keep track of our small form
   */
  const [search, setSearch] = React.useState("");
  const [category, setCategory] = React.useState("");

  /**
   * We want to get/save the results if the user reloads everything
   * I sure would not like to have all my favorites blown away
   * after leaving the page on accident
   */
  const itemsFromSesssion = JSON.parse(get("savedItems"));
  const [savedResults, setSavedResults] = React.useState(
    itemsFromSesssion || [],
  );

  /**
   * create a URL object for us to use in our API query
   */
  const url = new URL("https://pixabay.com/api/");
  const defaultParams = {
    key: SECRET_KEY,
  };
  url.search = new URLSearchParams(defaultParams);

  /**
   * Our hook, returns some needed info to keep our applications state running
   * and updating the view when we have the results we want
   */
  const { isLoading, response, error, setUrl } = useFetch(url, {
    method: "GET",
  });

  /**
   * Sets a new URL for our useFetch hook, which fires it due to
   * the query changing
   */
  const submitSearch = () => {
    // q is required by the api, short for query
    url.search = new URLSearchParams({
      ...defaultParams,
      q: search.trim(),
      category,
    });
    setUrl(url);
  };

  /**
   *
   * @param {Object} toSave
   * a util function to find a saved result
   */
  const findSavedResult = (toSave) => {
    return savedResults.find((el) => el.id === toSave.id);
  };

  /**
   *
   * @param {Object} toSave
   * the image that the user is selecting to save
   */
  const saveResult = (toSave) => {
    if (!findSavedResult(toSave)) {
      setSavedResults([...savedResults, toSave]);

      // send to localStorage
      setItem("savedItems", JSON.stringify([...savedResults, toSave]));
    }
  };

  return (
    <div className="App">
      <Column>
        <div className="search">
          <SearchForm
            categories={categories}
            classes={classes}
            setCategory={setCategory}
            setSearch={setSearch}
            submitSearch={submitSearch}
            category={category}
          />
          <ErrorBoundary error={error}>
            {isLoading && !Boolean(response.hits) ? (
              <Column style={{ alignItems: "center" }}>
                <CircularProgress size={100} variant="indeterminate" />
              </Column>
            ) : (
              <SearchResults
                classes={classes}
                hits={response.hits}
                saveResult={saveResult}
                findSavedResult={findSavedResult}
              />
            )}
          </ErrorBoundary>
          {response.hits && response.hits.length === 0 && !isLoading && (
            <div>
              <MoodBadIcon />
              We're sorry, maybe try a better search term?
            </div>
          )}
        </div>
      </Column>
      <div className="links">
        <SavedLinks savedResults={savedResults} />
      </div>
    </div>
  );
}

export default App;
