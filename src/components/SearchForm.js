import React from "react";

import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";

const SearchForm = ({
  categories,
  category,
  classes,
  setCategory,
  setSearch,
  submitSearch,
}) => {
  return (
    <>
      <FormControl className={classes.formControl}>
        <TextField
          label="Search"
          labelid="Search"
          onChange={(e) => setSearch(e.target.value)}
          onKeyPress={(e) => e.which === 13 && submitSearch()}
        />
      </FormControl>
      <Button
        color="primary"
        onClick={() => submitSearch()}
        size="medium"
        variant="contained"
        data-testid="SearchButton"
      >
        Search
      </Button>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="categories">Categories</InputLabel>
        <Select
          inputProps={{ id: "categories" }}
          label="Categories"
          onChange={(e) => setCategory(e.target.value)}
          value={category}
        >
          {categories.map((c) => (
            <MenuItem key={c.value} value={c.value}>
              {c.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  );
};
export default SearchForm;
