import React from "react";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import { Typography, Button, Chip, ListItem } from "@material-ui/core";
import StarIcon from "@material-ui/icons/Star";

import "./SearchResults.css";

const SearchResults = ({ hits = [], saveResult, findSavedResult, classes }) => {
  return (
    <>
      {hits.map(({ favorites, id, user, previewURL, tags, likes, ...rest }) => (
        <ListItem key={id}>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <img src={previewURL} alt={tags} width={125} height={125} />
            <Button
              className={classes.button}
              color="secondary"
              onClick={() => saveResult({ ...rest, id })}
              variant="contained"
            >
              {findSavedResult({ id }) ? "Saved" : "Save"}
            </Button>
          </div>
          <div className="item-details">
            <div className="tags">
              {tags.split(",").map((tag) => (
                <Chip key={tag} label={tag} size="small" />
              ))}
            </div>
            <div className="icon-group">
              <div className="icon">
                <Typography variant="srOnly">likes + {likes}</Typography>
                <span>{likes}</span> <ThumbUpIcon fontSize="small" />
              </div>
              <div className="icon">
                <Typography variant="srOnly">
                  favorites + {favorites}
                </Typography>
                <span>{favorites}</span> <StarIcon fontSize="small" />
              </div>
            </div>
          </div>
        </ListItem>
      ))}
    </>
  );
};

export default SearchResults;
