import React from "react";
import Column from "./Column";
import { Link } from "@material-ui/core";
import LinkIcon from "@material-ui/icons/Link";

const SavedLinks = ({ savedResults }) => {
  return (
    <Column>
      <h3>Saved</h3>
      {savedResults.map(({ id, webformatURL }) => (
        <Link key={id} target="_" href={webformatURL}>
          <div style={{ display: "flex", alignItems: "center" }}>
            #{id}
            <LinkIcon />
          </div>
        </Link>
      ))}
    </Column>
  );
};

export default React.memo(SavedLinks);
