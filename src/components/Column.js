import React from "react";

import "./Columns.css";

const Column = ({ children, ...props }) => {
  return (
    <div className="column" {...props}>
      {children}
    </div>
  );
};

export default Column;
