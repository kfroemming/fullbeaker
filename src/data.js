export const categories = [
  { value: "backgrounds", label: "backgrounds" },
  { value: "fashion", label: "fashion" },
  { value: "nature", label: "nature" },
  { value: "science", label: "science" },
  {
    value: "education",
    label: "education",
  },
  {
    value: "feelings",
    label: "feelings",
  },
  {
    value: "health",
    label: "health",
  },
  {
    value: "people",
    label: "people",
  },
  {
    value: "religion",
    label: "religion",
  },
  {
    value: "places",
    label: "places",
  },
  {
    value: "animals",
    label: "animals",
  },
  {
    value: "industry",
    label: "industry",
  },
  {
    value: "computer",
    label: "computer",
  },
  {
    value: "food",
    label: "food",
  },
  {
    value: "sports",
    label: "sports",
  },
  {
    value: "transportation",
    label: "transportation",
  },
  {
    value: "travel",
    label: "travel",
  },
  {
    value: "buildings",
    label: "buildings",
  },
  {
    value: "business",
    label: "business",
  },
  {
    value: "music",
    label: "music",
  },
];
