import React from "react";

const useFetch = (defaultUrl, options = {}) => {
  const [response, setResponse] = React.useState({});
  const [error, setError] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);
  const [url, setUrl] = React.useState(defaultUrl);

  React.useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      try {
        const res = await fetch(url, options);
        const json = await res.json();
        setResponse(json);
        setIsLoading(false);
      } catch (error) {
        setError(error);
      }
    };
    fetchData();
  }, [url]);
  return { response, error, isLoading, setUrl };
};

export default useFetch;
